First task of the Nubank's challenge.

The closeness object opens the file edge.txt and runs the Floyd-Warshall algorithm in order to get the path between all nodes.
Then it computes the Normalized Closeness Centrality (based on this slides http://cs.brynmawr.edu/Courses/cs380/spring2013/section02/slides/05\_Centrality.pdf)

Lastly, it prints out measure to the console.

In order to run, one should execute:
`$sbt run`
then select the option [2] closeness.Closeness
