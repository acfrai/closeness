package closeness

import java.io.FileNotFoundException
import java.io.IOException
import io.Source

object Closeness {
  def main(args: Array[String]) {
    // Reads the file containing the edges
    val source: Source = Source.fromFile("edges.txt")
    val lines: Array[String] = source.getLines().toArray

    // Gets the maximum value of vertex
    var numberOfVertex: Int = -1
    lines.map { x => {
        var aux: Array[String] = x.split(" ")
        if(aux.head.toInt > numberOfVertex) {
          numberOfVertex = aux.head.toInt
        } else if(aux.tail.head.toInt > numberOfVertex) {
          numberOfVertex = aux.tail.head.toInt
        }
      }
    }
    
    // Initializes the distances matrix
    var dist : Array[Array[Double]] = Array.ofDim(numberOfVertex + 1, numberOfVertex + 1)
    for(i <- 0 to numberOfVertex) {
      for(j <- 0 to numberOfVertex) {
        if(i == j) {
          dist(i)(j) = 0
        } else { 
          dist(i)(j) = Double.PositiveInfinity
        }
      }
    }
    
    // Sets the distances from the file read
    lines.map { x => { 
        var aux: Array[String] = x.split(" ")
        dist(aux.head.toInt)(aux.tail.head.toInt) = 1
        dist(aux.tail.head.toInt)(aux.head.toInt) = 1
      }
    }
    
    // Floyd-Warshall algorithm
    for(k <- 0 to numberOfVertex) {
      for(i <- 0 to numberOfVertex) {
        for(j <- 0 to numberOfVertex) {
          if(dist(i)(j) > dist(i)(k) + dist(k)(j)) {
            dist(i)(j) = dist(i)(k) + dist(k)(j)
          }
        }
      }
    }
    
    // Closeness centrality
    var closeCent : Array[Double] = Array.ofDim(numberOfVertex + 1)
    for(i <- 0 to numberOfVertex) {
      var alldist : Double = 0
      for(j <- 0 to numberOfVertex) {
        alldist += dist(i)(j)
      }
      closeCent(i) = (1 / alldist) / numberOfVertex
    }
    
    var closeCentSorted = closeCent.sorted
    for(i <- 0 to numberOfVertex)
      print(closeCentSorted(i) + " ")
  }
}