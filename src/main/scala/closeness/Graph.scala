package closeness

class Graph(numOfVertex : Int) {
  var numberOfVertex  : Int = numOfVertex
  var adjacencyMatrix : Array[Array[Int]] = Array.ofDim(numberOfVertex + 1, numberOfVertex + 1)
  var distanceMatrix  : Array[Array[Double]] = Array.ofDim(numberOfVertex + 1, numberOfVertex + 1)
  var closenessCentr  : Array[Double] = Array.ofDim(numberOfVertex + 1)
    
  for(i <- 0 to numberOfVertex) {
    for(j <- 0 to numberOfVertex) {
      if(i == j) {
        adjacencyMatrix(i)(j) = 0
        distanceMatrix(i)(j) = 0
      } else {
        adjacencyMatrix(i)(j) = -1
        distanceMatrix(i)(j) = Double.PositiveInfinity
      }
    } // end for j
    
    closenessCentr(i) = Double.PositiveInfinity
  } // end for j
    
  
  // Runs Floyd-Warshall algorithm
  def FloydWarshall() : Boolean = {
    
    for(i <- 0 to numberOfVertex) {
      for(j <- 0 to numberOfVertex) {
        if(adjacencyMatrix(i)(j) == -1) {
          distanceMatrix(i)(j) = Double.PositiveInfinity
        } else {
          distanceMatrix(i)(j) = adjacencyMatrix(i)(j)
        }
      } // end for j
    } // end for i
    
    for(k <- 0 to numberOfVertex) {
      for(i <- 0 to numberOfVertex) {
        for(j <- 0 to numberOfVertex) {
          if(distanceMatrix(i)(j) > distanceMatrix(i)(k) + distanceMatrix(k)(j)) {
            distanceMatrix(i)(j) = distanceMatrix(i)(k) + distanceMatrix(k)(j)
          } // end if
        } // end for j
      } // end for i
    } // end for k
    
    return true
  } // end FloydWarshall function
  
  def closenessCentrality() : Array[Double] = {
    
    for(i <- 0 to numberOfVertex) {
      var allDist : Double = 0
      
      for(j <- 0 to numberOfVertex) {
        allDist += distanceMatrix(i)(j)
      } // end for j
      
      closenessCentr(i) = ((1 / allDist) / numberOfVertex)
    } // end for i
    
    closenessCentr
  } // end closenessCentrality function
  
  // Adds an edge to the graph
  def addEdge(origin : Int, dest : Int) : Boolean = {
    
    var max : Int = -1
    // Checking for new 
    if(origin > numberOfVertex) {
      max = origin
    } // end if origin > numberOfVertex
    
    if(dest > numberOfVertex && dest > origin) {
      max = dest
    } // end if dest > numberOfVertex and dest > origin
      
    if(max != -1) {
      var adjacencyMatrixAux : Array[Array[Int]] = Array.ofDim(max + 1, max + 1)
      
      for(i <- 0 to numberOfVertex) {
        for(j <- 0 to numberOfVertex) {
          adjacencyMatrixAux(i)(j) = adjacencyMatrix(i)(j)
        } // end for i
      } // end for j
      
      adjacencyMatrix = Array.ofDim(max + 1, max + 1)
      for(i <- 0 to max) {
        for(j <- 0 to max) {
          adjacencyMatrix(i)(j) = adjacencyMatrixAux(i)(j)
        } // end for i
      } // end for j
    } // end if max != -1
    
    numberOfVertex = max
    adjacencyMatrix(origin)(dest) = 1
    adjacencyMatrix(dest)(origin) = 1
    return true
  } // end addEdge function
  
  def flagAsFraudulent(vertex : Int) : Unit = {
    closenessCentr(vertex) = 0
    
    for(i <- 0 to numberOfVertex) {
      if(adjacencyMatrix(vertex)(i) == 1) {
        closenessCentr(vertex) = closenessCentr(vertex) / 2.0
      } else if(adjacencyMatrix(vertex)(i) > 1) {
        closenessCentr(i) = closenessCentr(i) * (1 - Math.pow(0.5, adjacencyMatrix(vertex)(i)))
      } // end else if
    } // end for i
  } // end flagAsFraudulent function
} // end graph class