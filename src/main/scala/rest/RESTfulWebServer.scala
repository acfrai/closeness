package rest

import akka.actor.Actor
import spray.routing._
import spray.http._
import MediaTypes._
import closeness.Graph
import java.io.FileNotFoundException
import java.io.IOException
import io.Source

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class MyServiceActor extends Actor with MyService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing
  // or timeout handling
  def receive = runRoute(myRoute)
}


// this trait defines our service behavior independently from the service actor
trait MyService extends HttpService {
  
  // Reads the file containing the edges
  val source: Source = Source.fromFile("edges.txt")
  val lines: Array[String] = source.getLines().toArray

  // Gets the maximum value of vertex
  var max: Int = -1
  lines.map { x => {
      var aux: Array[String] = x.split(" ")
      if(aux.head.toInt > max) {
        max = aux.head.toInt
      } else if(aux.tail.head.toInt > max) {
        max = aux.tail.head.toInt
      }
    }
  }
  
  var g : Graph = new Graph(max)
  // Sets the distances from the file read
  lines.map { x => { 
      var aux: Array[String] = x.split(" ")
      g.addEdge(aux.head.toInt, aux.tail.head.toInt)
    }
  }
  g.FloydWarshall()
  var a : Array[Double] = g.closenessCentrality().sorted
//  for(i <- 0 to max) {
//      print(a(i) + " ")
//  }
  
  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) { // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <h1>Closeness Centrality Challenge</h1>
								
									Insert Edge:
									Origin <input type="number" name="origin" min="0"></input>
									Destination <input type="number" name="dest" min="0"></input>
									<input type="add" value="Add"></input>
									Flag vertex as fraudulent: <input type="number" name="fraudulent" min="0"></input><input type="fraudulent" value="Submit"></input>
              </body>
            </html>
          }
        }
      }
    }
}